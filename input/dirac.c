/*****************************************************************************
* dirac.c: Dirac parser.
*****************************************************************************
* Copyright (C) 2009
*
* Authors: Nathan Caldwell <saintdev@gmail.com>
*          The authors of schroedinger-tools.
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111, USA.
*****************************************************************************/

#include <stdio.h>
#include <string.h>
#include <schroedinger/schro.h>
#include "common.h"
#include "dirac.h"

typedef struct {
	FILE *fh;
	SchroDecoder *p_schro;
	SchroVideoFormat *p_format;
} dirac_input_t;

#define DIRAC_PARSE_MAGIC "BBCD"

int parse_packet( dirac_input_t *h, uint8_t **pp_data, int *p_size );
static void buffer_free( SchroBuffer *buf, void *priv );

int open_file_dirac( char *psz_filename, hnd_t *p_handle, config_t *p_config )
{
	int size = -1;
	int it;
	uint8_t *packet = NULL;
	dirac_input_t *h = calloc(1, sizeof(*h));
	SchroBuffer *buffer;
	
	if( !strcmp(psz_filename, "-") )
		h->fh = stdin;
	else
		h->fh = fopen(psz_filename, "rb");
	if( h->fh == NULL )
		return -1;

	if( parse_packet( h, &packet, &size ) )
		return -1;

	if( size == 0 )
		return -1;

	schro_init();

	h->p_schro = schro_decoder_new();

	buffer = schro_buffer_new_with_data( packet, size );
	buffer->free = buffer_free;
	buffer->priv = packet;

	it = schro_decoder_push( h->p_schro, buffer );
	if( it == SCHRO_DECODER_FIRST_ACCESS_UNIT )
	{
		h->p_format = schro_decoder_get_video_format( h->p_schro );
	}

	if( h->p_format->interlaced )
		fprintf( stderr, "warning: sequence may be interlaced!\n" );

	p_config->i_width = h->p_format->width;
	p_config->i_height = h->p_format->height;
	switch(h->p_format->chroma_format) {
		case SCHRO_CHROMA_420:
			p_config->i_csp = COLORSPACE_420;
			break;
		case SCHRO_CHROMA_444:
		case SCHRO_CHROMA_422:
			fprintf( stderr, "ERROR: Unsupported chroma format.\n" );
			return -1;
	}

	*p_handle = (hnd_t)h;
	return 0;
}

/* Lots of this is from schroedinger-tools */
int read_frame_dirac( hnd_t handle, picture_t *p_pic, int i_frame )
{
	dirac_input_t *h = handle;
	int size = -1;
	uint8_t *packet;
	SchroBuffer *buffer;
	SchroFrame *frame;
	int go = 1;

	/* This function assumes that it will be called with i_frame increasing. */
	schro_decoder_set_earliest_frame( h->p_schro, i_frame );

	while( 1 ) {

		/* Handle EOF from previous iteration */
		if( size == 0 )
			break;

		go = 1;
		while( go ) {
			switch( schro_decoder_wait( h->p_schro ) ) {
				case SCHRO_DECODER_NEED_BITS:
					go = 0;
					break;
				case SCHRO_DECODER_NEED_FRAME:
					switch(h->p_format->chroma_format) {
						case SCHRO_CHROMA_444:
							frame = schro_frame_new_and_alloc( NULL, SCHRO_FRAME_FORMAT_U8_444,
															   h->p_format->width, h->p_format->height );
							break;
						case SCHRO_CHROMA_422:
							frame = schro_frame_new_and_alloc( NULL, SCHRO_FRAME_FORMAT_U8_422,
															   h->p_format->width, h->p_format->height );
							break;
						case SCHRO_CHROMA_420:
							frame = schro_frame_new_and_alloc( NULL, SCHRO_FRAME_FORMAT_U8_420,
															   h->p_format->width, h->p_format->height );
							break;
						default:
							printf("ERROR: unsupported chroma format\n");
							return -1;
					}
					schro_decoder_add_output_picture( h->p_schro, frame );
					break;
				case SCHRO_DECODER_OK:
					{
						int i_dts = schro_decoder_get_picture_number( h->p_schro );
						frame = schro_decoder_pull( h->p_schro );
						if( i_dts != i_frame )
						{
							/* This shouldn't happen, why does it? */
							schro_frame_unref(frame);
							break;
						}

						memcpy( p_pic->img.plane[0], frame->components[0].data, frame->components[0].length );
						memcpy( p_pic->img.plane[1], frame->components[1].data, frame->components[1].length );
						memcpy( p_pic->img.plane[2], frame->components[2].data, frame->components[2].length );

						schro_frame_unref(frame);
						return 0;
					}
					break;
				case SCHRO_DECODER_EOS:
					schro_decoder_reset( h->p_schro );
					go = 0;
					break;
				case SCHRO_DECODER_ERROR:
					break;
			}
		}

		if( parse_packet( h, &packet, &size ) )
		{
			break;
		}
		
		if( size == 0 ) {
			/* Unexpected EOF */
			schro_decoder_push_end_of_stream( h->p_schro );
			return -1;
		} else {
			buffer = schro_buffer_new_with_data( packet, size );
			buffer->free = buffer_free;
			buffer->priv = packet;
			
			schro_decoder_push( h->p_schro, buffer );
		}
		
	}

	return 0;
}

int close_file_dirac( hnd_t handle )
{
	dirac_input_t *h = handle;
	if( !h || !h->fh || !h->p_schro || !h->p_format )
		return 0;
	fclose( h->fh );
	free( h->p_format );
	schro_decoder_free( h->p_schro );
	free( h );
	return 0;
}

/* From schroedinger-tools */
int parse_packet( dirac_input_t *h, uint8_t **pp_data, int *p_size )
{
	uint8_t *packet;
	uint8_t header[13];
	int n;
	int size;
	
	n = fread( header, 1, 13, h->fh );
	if( feof( h->fh ) ) {
		*pp_data = NULL;
		*p_size = 0;
		return 0;
	}
	if( n < 13 ) {
		fprintf( stderr, "ERROR: truncated header\n" );
		return -1;
	}
	
	if( strncmp((char *)header, DIRAC_PARSE_MAGIC, strlen(DIRAC_PARSE_MAGIC)) ) {
		fprintf( stderr, "ERROR: header magic incorrect\n" );
		return -1;
	}
	
	size = (header[5]<<24) | (header[6]<<16) | (header[7]<<8) | (header[8]);
	if ( size == 0 ) {
		size = 13;
	}
	if( size < 13 ) {
		fprintf( stderr, "ERROR: packet too small? (%d)\n", size );
		return -1;
	}
	if( size > 1<<24 ) {
		fprintf( stderr, "ERROR: packet too large? (%d > 1<<24)\n", size );
		return -1;
	}
	
	packet = malloc( size );
	memcpy( packet, header, 13 );
	n = fread( packet + 13, 1, size - 13, h->fh );
	if( n < size - 13 ) {
		free( packet );
		return -1;
	}

	*pp_data = packet;
	*p_size = size;
	return 0;
}

static void buffer_free( SchroBuffer *buf, void *priv )
{
	free (priv);
}
