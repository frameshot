/*****************************************************************************
* output.c: output drivers.
*****************************************************************************
* Copyright (C) 2009
*
* Authors: Nathan Caldwell <saintdev@gmail.com>
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111, USA.
*****************************************************************************/

#include <stdint.h>
#include <malloc.h>
#include <libavutil/avutil.h>
#include <libswscale/swscale.h>
#include <png.h>

#include "common.h"
#include "output.h"

typedef struct {
	FILE *fh;
	png_structp p_png;
	png_infop   p_info;
} png_output_t;

int open_file_png(char *psz_filename, hnd_t *p_handle, int i_compression )
{
	png_output_t *h = NULL;
	if( (h = calloc(1, sizeof(*h))) == NULL )
		return -1;

	if( !strcmp(psz_filename, "-") )
		h->fh = stdout;
	else if( (h->fh = fopen(psz_filename, "wb")) == NULL )
	{
		goto error;
	}

	if( (h->p_png = png_create_write_struct( PNG_LIBPNG_VER_STRING, NULL, NULL, NULL )) == NULL )
	{
		goto error;
	}

	if( (h->p_info = png_create_info_struct(h->p_png)) == NULL )
	{
		png_destroy_write_struct(&(h->p_png), (png_infopp)NULL);
		goto error;
	}

	png_init_io( h->p_png, h->fh );

	png_set_compression_level( h->p_png, i_compression );

	*p_handle = h;

	return 0;

error:
	if( h->fh != NULL && h->fh != stdout )
		fclose( h->fh );
	free(h);

	return -1;
}

int close_file_png( hnd_t handle )
{
	int ret = 0;
	png_output_t *h = handle;

	png_destroy_write_struct( &(h->p_png), &(h->p_info) );

	if ((h->fh == NULL) || (h->fh == stdout))
		return ret;

	ret = fclose(h->fh);

	free(h);

	return ret;
}

int write_image_png( hnd_t handle, picture_t *p_pic, config_t *p_config )
{
	png_output_t *h = handle;
	uint8_t *out_data = malloc(p_config->i_width * p_config->i_height * 4);
	uint8_t **pp_rows = calloc(p_config->i_height, sizeof(*pp_rows));
	picture_t pic_out;
	struct SwsContext *p_sws;
	int i;

	pic_out.img.plane[0] = out_data;
	pic_out.img.plane[1] = pic_out.img.plane[2] = pic_out.img.plane[3] = NULL;
	pic_out.img.i_stride[0] = 3*p_config->i_width;
	pic_out.img.i_stride[1] = pic_out.img.i_stride[2] = pic_out.img.i_stride[3] = 0;

	p_sws = sws_getContext( p_config->i_width, p_config->i_height, PIX_FMT_YUV420P,
							p_config->i_width, p_config->i_height, PIX_FMT_RGB24,
							SWS_FAST_BILINEAR | SWS_ACCURATE_RND,
							NULL, NULL, NULL );

	sws_scale(p_sws, p_pic->img.plane, p_pic->img.i_stride, 0, p_config->i_height, pic_out.img.plane, pic_out.img.i_stride );

	__asm__ volatile ("emms\n\t");

	
	png_set_IHDR( h->p_png, h->p_info, p_config->i_width, p_config->i_height,
				  8, PNG_COLOR_TYPE_RGB, PNG_INTERLACE_NONE,
				  PNG_COMPRESSION_TYPE_DEFAULT, PNG_FILTER_TYPE_DEFAULT );
	
	for(i=0; i < p_config->i_height; i++)
		pp_rows[i] = pic_out.img.plane[0] + i * pic_out.img.i_stride[0];

	png_set_rows( h->p_png, h->p_info, pp_rows );

	png_write_png( h->p_png, h->p_info, 0, NULL );

	free(pp_rows);
	free(out_data);
	
	return 0;
}
