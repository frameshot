/*****************************************************************************
* output.h: output drivers.
*****************************************************************************
* Copyright (C) 2009
*
* Authors: Nathan Caldwell <saintdev@gmail.com>
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111, USA.
*****************************************************************************/

int open_file_png(char *psz_filename, hnd_t *p_handle, int i_compression );
int write_image_png( hnd_t handle, picture_t *p_pic, config_t *p_config );
int close_file_png( hnd_t handle );
