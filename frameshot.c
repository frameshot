/*****************************************************************************
* frameshot: a frame-accurate screenshot generator.
*****************************************************************************
* Copyright (C) 2009
*
* Authors: Nathan Caldwell <saintdev@gmail.com>
*          Various parts taken from x264.
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111, USA.
*****************************************************************************/

#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <limits.h>

#include <zlib.h>


#define _GNU_SOURCE
#include <getopt.h>

#include "common.h"
#include "utils.h"
#include "output.h"
#include "input.h"

enum
{
	FORMAT_UNKNOWN,
	FORMAT_H264,
	FORMAT_DIRAC,
	FORMAT_OGG,
	FORMAT_M4V
};

typedef struct {
	char *psz_outdir;
	int i_zlevel;
	hnd_t hin;
} cli_opt_t;

/* input file function pointers */
int (*p_open_infile)( char *psz_filename, hnd_t *p_handle, config_t *p_config );
int (*p_read_frame)( hnd_t handle, picture_t *p_pic, int i_frame );
int (*p_close_infile)( hnd_t handle );

/* output file function pointers */
static int (*p_open_outfile)( char *psz_filename, hnd_t *p_handle, int i_compression );
// static int (*p_set_outfile_param)( hnd_t handle, config_t *p_config );
static int (*p_write_image)( hnd_t handle, picture_t *p_pic, config_t *p_config );
static int (*p_close_outfile)( hnd_t handle );

static int parse_options( int argc, char **argv, config_t *config, cli_opt_t *opt );
static int grab_frames( config_t *config, cli_opt_t *opt );

int main(int argc, char **argv)
{
	config_t config;
	cli_opt_t opt;
	int ret = 0;

	parse_options(argc, argv, &config, &opt);

	ret = grab_frames( &config, &opt );

	return ret;
}

static void show_help(void)
{
	#define HELP printf
	HELP( "Syntax: frameshot [options] infile\n"
	"\n"
	"Infile is a raw bitstream of one of the following codecs:\n"
	"  YUV4MPEG(*.y4m), Dirac(*.drc)\n"
	"\n"
	"Options:\n"
	"\n"
	"  -h, --help                  Displays this message.\n"
	);
	HELP( "  -f, --frames <int,int,...>  Frames numbers to grab.\n" );
	HELP( "  -o, --outdir <string>       Output directory for images.\n" );
	HELP( "  -z, --compression <integer> Ammount of compression to use.\n" );
	HELP( "  -1, --fast                  Use fastest compression.\n" );
	HELP( "  -9, --best                  Use best (slowest) compression.\n" );
	HELP( "\n" );
}

static int parse_options( int argc, char **argv, config_t *config, cli_opt_t *opt )
{
	char *psz_filename = NULL;
	int i_zlevel = Z_DEFAULT_COMPRESSION;
	char *psz, *psz_token;
	int b_y4m = 0;
	int b_dirac = 0;
	struct stat sb;

	memset( opt, 0, sizeof(*opt) );

	/* Default input driver */
	p_open_infile = open_file_y4m;
	p_read_frame = read_frame_y4m;
	p_close_infile = close_file_y4m;
	
	/* Default output driver */
	p_open_outfile = open_file_png;
	p_write_image = write_image_png;
	p_close_outfile = close_file_png;

	for( ;; )
	{
		int long_options_index = -1;
		static struct option long_options[] =
		{
			{ "fast", no_argument, NULL, '1' },
			{ "best", no_argument, NULL, '9' },
			{ "frames", required_argument, NULL, 'f' },
			{ "help", no_argument, NULL, 'h' },
			{ "outdir", required_argument, NULL, 'o' },
			{ "compression", required_argument, NULL, 'z' },
			{0, 0, 0, 0}
		};

		int c = getopt_long( argc, argv, "19f:ho:z:",
							 long_options, &long_options_index);

		if( c == -1 )
		{
			break;
		}

		switch( c )
		{
			case '1':
				i_zlevel = Z_BEST_SPEED;
				break;
			case '9':
				i_zlevel = Z_BEST_COMPRESSION;
				break;
			case 'f':
				for( config->i_frames = 0; config->i_frames < MAX_FRAMES; config->i_frames++, optarg = NULL )
				{
					psz_token = strtok(optarg, ",");
					if( psz_token == NULL )
						break;
					config->frames[config->i_frames] = atoi(psz_token);
				}
				qsort(config->frames, config->i_frames, sizeof(*config->frames), intcmp);
				break;
			case 'o':
				opt->psz_outdir = strdup(optarg);
				if( stat( opt->psz_outdir, &sb ) < 0 )
				{
					if( mkdir( opt->psz_outdir, S_IRWXU ) < 0 )
					{
						fprintf( stderr, "ERROR: Unable to create output directory.\n" );
						perror("mkdir");
						return -1;
					}
				} else {
					if( !S_ISDIR(sb.st_mode) )
					{
						fprintf(stderr, "ERROR: Outdir exists, and is not a directory." );
						return -1;
					}
				}
				break;
			case 'z':
				if( optarg == NULL || optarg[0] < '0' || optarg[0] > '9' )
				{
					opt->i_zlevel = Z_DEFAULT_COMPRESSION;
					break;
				}
				opt->i_zlevel = atoi(optarg);
				break;
			case 'h':
			default:
				show_help();
				exit(0);
		}
	}

	/* Get the input file name */
	if( optind > argc - 1 )
	{
		fprintf( stderr, "ERROR: No input file.\n" );
		show_help();
		return -1;
	}
	psz_filename = argv[optind++];

	psz = strrchr( psz_filename, '.' );
	if( !strncasecmp( psz, ".y4m", 4 ) )
		b_y4m = 1;
	else if( !strncasecmp( psz, ".drc", 4 ) )
		b_dirac = 1;

	if( !opt->psz_outdir )
		opt->psz_outdir = getcwd(NULL, 0);

	if( b_y4m )
	{
		p_open_infile = open_file_y4m;
		p_read_frame = read_frame_y4m;
		p_close_infile = close_file_y4m;
	}

	if( b_dirac )
	{
		p_open_infile = open_file_dirac;
		p_read_frame = read_frame_dirac;
		p_close_infile = close_file_dirac;
	}

	if( p_open_infile( psz_filename, &opt->hin, config ) )
	{
		fprintf( stderr, "ERROR: could not open input file '%s'\n", psz_filename );
		return -1;
	}

	return 0;
}

static int grab_frames( config_t *config, cli_opt_t *opt )
{
	hnd_t hout;
	picture_t pic;
	int i;
	char tmp[PATH_MAX];

	pic.img.plane[0] = calloc(1, 3 * config->i_width * config->i_height / 2);
	pic.img.plane[1] = pic.img.plane[0] + config->i_width * config->i_height;
	pic.img.plane[2] = pic.img.plane[1] + config->i_width * config->i_height / 4;
	pic.img.plane[3] = NULL;
	
	pic.img.i_stride[0] = config->i_width;
	pic.img.i_stride[1] = pic.img.i_stride[2] = config->i_width / 2;
	pic.img.i_stride[3] = 0;

	for(i = 0; i < config->i_frames; i++ )
	{
		p_read_frame( opt->hin, &pic, config->frames[i] );
		
		snprintf(tmp, PATH_MAX, "%s/%05d.png", opt->psz_outdir, config->frames[i]);
		p_open_outfile( tmp, &hout, opt->i_zlevel );
		p_write_image( hout, &pic, config );
		p_close_outfile( hout );
	}

	p_close_infile( opt->hin );

	if( opt->psz_outdir )
		free(opt->psz_outdir);
	
	return 0;
}
